import csv
import datetime
from decimal import Decimal

def growth(most_recent, second_most_recent):
    net_change = float(most_recent - second_most_recent)
    return round(Decimal(net_change / second_most_recent * 100))

def reverse_order(textfile):
    return reversed(list(csv.reader(textfile)))

def get_case_count(row):
    return float(row[2])

def output_topline(state_to_search, percent_grown, date_string):
    print("The number of covid cases for %s grew %.f%% for %s" % (state_to_search, percent_grown, date_string))

def output_bottom_line(trailing_case_count, lead_case_count):
    print("(from %.f cases to %.f cases) \n" % (trailing_case_count, lead_case_count))


def parse_tracking_project(state_to_search):
    CASES_TO_FIND = 7
    with open('./states-daily.csv', 'r') as textfile:
        case_data = csv.reader(textfile)
        # skip the header row
        found_count = 0
        done = object()
        row = next(case_data, done);
        while (row is not done) and found_count < CASES_TO_FIND:
            name = row[1]
            if name == state_to_search:
                if (found_count == 0):
                    lead_case = row
                elif (found_count > 0):
                    trailing_case = row

                    lead_case_count = get_case_count(lead_case)
                    trailing_case_count = get_case_count(trailing_case)
                    percent_grown = growth(lead_case_count, trailing_case_count)
                    datetime_object = datetime.datetime.strptime(lead_case[0], '%Y%m%d')

                    output_topline(state_to_search, percent_grown, datetime_object.strftime("%A, %B %d, %Y"))
                    output_bottom_line(trailing_case_count, lead_case_count)
                    if CASES_TO_FIND > 2:
                        lead_case = row
                found_count += 1
            row = next(case_data, done)

#
#def main():
#    parse_tracking_project('VA')
#    with open('../covid-19-data/us-states.csv', 'r') as textfile:
#        backwards = reverse_order(textfile)
#        case_growth = growth(739, 604)
#        print case_growth;
#        for row in backwards:
#            name = row[1]
#            if name == 'Virginia':
#                print ', '.join(row)

parse_tracking_project('VA')

