## Covid Rates!

Using data from the [Covid Tracking Project](https://covidtracking.com/) 
to check the percentage increase (eventually decrease?) in new cases reported per state.